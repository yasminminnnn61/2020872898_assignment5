/*
        Name: NUR YASMIN BINTI MOHD KAHAR
        Date: 1/7/2021
 */
function calc() 
{
    var name = document.forms["parcel"]["name"].value;
    var length = document.forms["parcel"]["length"].value;
    var width = document.forms["parcel"]["width"].value;
    var height = document.forms["parcel"]["height"].value;

    var mode = document.forms["parcel"]["mode"].value;
    var type = document.forms["parcel"]["type"].value; 

    var weight= (length*width*height)/5000;

    var total;

    if(type=="Domestic")
{

        if(weight<2.00)
        {

            if(mode=="Surface")
            {
                total = 7.00;
            }
            else if (mode=="Air")
            {
                total = 10.00;
            }
        }
        else if (weight >=2.00)
         {

            if(mode=="Surface")
            {
                total = 7.00 + ( ( weight - 2.00 ) * 1.50 ); 
            }
            else if (mode=="Air")
            {
                total = 10.00 + ( ( weight - 2.00 ) * 3.00 );
            }
        }
}      

    else 
{

       if(weight<2)
       {  
            if(mode=="Surface")
            {
                total = 20.00;
            }
            else if(mode=="Air")
            {
                total=50.00;
            }
        }
        else if(mode>=2.00)
        {

            if(mode=="Surface")
            {
                total = 20.00 + ( ( weight - 2.00 ) * 3.00 );
            }
            else if(mode=="Air")
            {
                total = 50.00 + ( ( weight - 2.00 ) * 5.00 );
            }
        }
} 
alert("Parcel Volumetric and Cost Calculator " +"\n" + "Customer Name: " +name+ "\n" + "Length: " + length + "cm" + "\n" + "Width: "+width+"cm" + "\n" +  "Height: "+height+"cm" + "\n" + "Weight: " +weight+"kg" + "\n" +  "Mode: "+mode+"\n" +  "Type: " +type+ "\n" + "Delivery cost: RM "+total);   
}

function set()
{
    alert("The inputs will be reset");
}

function uppercase()
{
    var name = document.forms["parcel"]["name"];
    name.value = name.value.toUpperCase();  
}